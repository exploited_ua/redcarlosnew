$(document).ready(function () {
  var geocoder;
  var map;
  var address = $('li.address').text();
  var styles = [
    {"featureType" : "administrative", "elementType" : "labels.text.fill", "stylers" : [
      {"color" : "#444444"}
    ]},
    {"featureType" : "landscape", "elementType" : "all", "stylers" : [
      {"color" : "#f2f2f2"}
    ]},
    {"featureType" : "poi", "elementType" : "all", "stylers" : [
      {"visibility" : "off"}
    ]},
    {"featureType" : "road", "elementType" : "all", "stylers" : [
      {"saturation" : -100},
      {"lightness" : 45}
    ]},
    {"featureType" : "road.highway", "elementType" : "all", "stylers" : [
      {"visibility" : "simplified"}
    ]},
    {"featureType" : "road.arterial", "elementType" : "labels.icon", "stylers" : [
      {"visibility" : "off"}
    ]},
    {"featureType" : "transit", "elementType" : "all", "stylers" : [
      {"visibility" : "off"}
    ]},
    {"featureType" : "water", "elementType" : "all", "stylers" : [
      {"color" : "#ff7563"},
      {"visibility" : "on"}
    ]}
  ];

  function initialize() {
    geocoder = new google.maps.Geocoder();
      var latlng = new google.maps.LatLng(50.419126, 30.5132053);
    var styledMap = new google.maps.StyledMapType(styles, {name : "Styled Map"});
    var myOptions = {
      zoom : 11,
      center : latlng,
      mapTypeControl : false,
      scrollwheel : false,
      mapTypeControlOptions : {
        style : google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        mapTypeIds : [google.maps.MapTypeId.ROADMAP, 'map_style']
      },
      navigationControl : true,
      mapTypeId : google.maps.MapTypeId.ROADMAP

    };
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');

    if (geocoder) {
      geocoder.geocode({
        'address' : address
      }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
            map.setCenter(results[0].geometry.location);

            var infowindow = new google.maps.InfoWindow({
              content : '<b>' + address + '</b>',
              size : new google.maps.Size(150, 50)
            });

            var marker = new google.maps.Marker({
              position : results[0].geometry.location,
              map : map,
              title : address
            });
            google.maps.event.addListener(marker, 'click', function () {
              infowindow.open(map, marker);
            });

          } else {
            alert("No results found");
          }
        } else {
          alert("Geocode was not successful for the following reason: " + status);
        }
      });
    }
  }

  google.maps.event.addDomListener(window, 'load', initialize);
});

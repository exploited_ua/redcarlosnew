$(document).ready(function() {
	$(window).on('scroll', function() {
	    var scrollTop = $(this).scrollTop();
			if ( scrollTop > 700 ) {
				$('.navigation-menu').addClass('is-fixed').removeClass('nav-menu-animate');
				$('#overlay .loading').hide();
				return;
			}
			else if(scrollTop < 700 ){
				$('.navigation-menu').removeClass('is-fixed').removeClass('nav-menu-animate');
				$('.social-button').removeClass('animate-soc-button');
				$('#overlay .loading').show();
			}
			// alert(scrollTop);
	});
	$('.open-menu').on('click', function(){
		$('.navigation-menu').toggleClass('nav-menu-animate');
		$('.social-button').toggleClass('animate-soc-button');
	});
	$('.close-navigation-menu').on('click', function(){
			$('.navigation-menu').toggleClass('nav-menu-animate');
			$('.social-button').removeClass('animate-soc-button');
	});


	$('.overlay').click(function() {
		$(this).remove();
	});

	$("#click").click(function (){
			$('html, body').animate({
					scrollTop: $("#cd-placeholder-2").offset().top
			}, 500);
	});

		$(function() {
      $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
      });
    });

	var lastScrollTop = 0;

	$('#portfolio figcaption a').on("click",function(e){
		e.preventDefault();
		var modalDialog = $('#modalDialog');
		$('#overlay').show();
		modalDialog.fadeIn(400);
		modalDialog.find('img.website').attr('src',$(this).attr('href'));
		lastScrollTop = $('body').scrollTop();

		$('body').scrollTop(0);

	});

	$('.close').on("click",function(){
		$('body').scrollTop(lastScrollTop);
		var modalDialog = $(this).parent();
		modalDialog.fadeOut();
		$('#overlay').hide();
		modalDialog.find('img.website').attr('src','');
	});

	$('#showOrderForm,#showOrderForm2,#showOrderForm3').on('click',function(e){
		e.preventDefault();
		$('.fs-form-full-modal').show();
	});


 });


(function() {
	var formWrap = document.getElementById( 'fs-form-wrap' );
	new FForm( formWrap, {
		onReview : function() {
			classie.add( document.body, 'overview' );
		}
	} );
})();
